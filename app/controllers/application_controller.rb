class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

   layout :layout


  def layout
    if devise_controller?
      'admin_login'
    else
      'admin'
    end
  end

  def home

  end
end
