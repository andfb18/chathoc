class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  dragonfly_accessor :photo

  def avatar_url(width = nil, height = nil)
    generate_avatar
    
    # @ToDo the host should be added only for mail,s investigate more about dragonfly configuration
    if ( width && height ) 
      photo.thumb("#{width}x#{height}").url host: Rails.application.config.action_mailer.asset_host
    else
      photo.url host: Rails.application.config.action_mailer.asset_host
    end
  rescue => e
    logger.error " ==== ERROR GENERATING IMAGE, CHECK user#avatar_url"
    logger.error e.message
    ActionController::Base.helpers.asset_path 'default-user.jpg'
  end

  def generate_avatar(force = false)
    if force || photo.nil?
      avatar = Dragonfly.app.generate(:initial_avatar, "#{first_name} #{last_name}", self.class::AVATAR_OPTIONS)
      update_attribute(:photo, avatar)
    end
  end
end
