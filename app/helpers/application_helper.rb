module ApplicationHelper
  
  def body_classes
    path = self.controller_path.split('/')
    namespace = path.second ? path.first : nil
    controller_name = params[:controller].gsub('/', '_')
    "#{namespace} controller_#{controller_name} action_#{params[:action]}"
  end
  
  def title(page_title)
    content_for(:title) { page_title }
  end
    
  def boolean_check(value)
    css_class = {
        NilClass => 'default',
      FalseClass => 'danger',
       TrueClass => 'success'
    }[value.class]

    icon = {
        NilClass => 'minus',
      FalseClass => 'ban',
       TrueClass => 'check'
    }[value.class]

    content_tag :span, class: "label label-#{css_class}" do
      content_tag :i, '', class: "fa fa-#{icon}"
    end
  end

  def badge(text = false, type: 'primary', &block)
    content_tag :span, class: "badge badge-#{type}" do
      block ? block.call : text
    end
  end

  def bs_label(text, type = 'primary', &block)
    content_tag :span, class: "label label-#{type}" do
      block ? block.call : text
    end
  end

  def progress_bar(percentage, type: nil)
    if type.nil?
      type = if percentage > 80 && percentage < 100
        'warning'
      elsif percentage >= 100
        'danger'
      else
        'info'
      end
  end
  
  # ToDO check what does this outside of a method... ?
  content_tag :div, class: 'progress' do
      tag :div, {
        class: "progress-bar progress-bar-#{type} animate-progress-bar",
        'data-percentage': "#{percentage}%"
      }

    end
  end
end
