

function include(url){
  document.write('<script src="'+url+'"></script>');
  return false ;
}

/* wow.JS
========================================================*/
/*include('javascripts/wow.js');*/

/* DEVICE.JS
========================================================*/
// include('javascripts/smooth-page-scrolling/device.min.js');
/* Easing library
========================================================*/
// include('javascripts/smooth-page-scrolling/jquery.easing.1.3.js');
/* DEVICE.JS AND SMOOTH SCROLLIG
========================================================*/
// include('javascripts/smooth-page-scrolling/jquery.mousewheel.min.js');
// include('javascripts/smooth-page-scrolling/jquery.simplr.smoothscroll.min.js');

$(function () { 
  if ($('html').hasClass('desktop')) {
      $.srSmoothscroll({
        step:190,
        speed:900
      });
  }   
});