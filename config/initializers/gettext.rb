FastGettext.add_text_domain 'app', :path => 'config/locales'
FastGettext.default_available_locales = ['en','es']
FastGettext.default_text_domain = 'app'

Haml::Template.enable_magic_translations(:fast_gettext) 

Rails.application.config.gettext_i18n_rails.default_options = %w[--sort-by-msgid --no-wrap]