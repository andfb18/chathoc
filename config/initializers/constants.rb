EMAIL_REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i
# 3 numbers followed by number - or space min 3 times
PHONE_REGEX = /\d{3}[\d -]{3,}/