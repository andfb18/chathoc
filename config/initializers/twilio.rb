require 'twilio-ruby'

Twilio.configure do |config|
  config.account_sid = Rails.application.secrets.twilio['account_sid'] rescue nil
  config.auth_token = Rails.application.secrets.twilio['auth_token'] rescue nil
end
