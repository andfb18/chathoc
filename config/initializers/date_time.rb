# Date
Date::DATE_FORMATS[:default] = "%d/%m/%Y" 

# Time
Time::DATE_FORMATS[:default] = "%d/%m/%Y %I:%M %p" 


class ActiveSupport::TimeWithZone
    def as_json(options = {})
        strftime('%Y-%m-%d %H:%M:%S')
    end
end