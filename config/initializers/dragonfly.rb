require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick
  plugin :avatarmagick, background_color: '98bf11', color: 'FFFFFF', size: '120x120'

  secret "f89awfu9823iofahf9374h837gfolaiu34hflo3478fhao3487fff"

  url_format "/media/:job/:name"
  url_host Rails.application.config.action_mailer.asset_host

  datastore :file,
    root_path: Rails.root.join('public/system/dragonfly', Rails.env),
    server_root: Rails.root.join('public')
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
if defined?(ActiveRecord::Base)
  ActiveRecord::Base.extend Dragonfly::Model
  ActiveRecord::Base.extend Dragonfly::Model::Validations
end
