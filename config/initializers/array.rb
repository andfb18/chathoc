module CoreExtensions
  module Array
    module ActiveSupportHelper

      # Convert an array I want to convert:
      #   [:one, :two, :three]
      # to:  
      #   {one: :one, two: :two, three: three}
      # currently used in model enum to store fields as text in db
      def to_h_for_enum
        self.map{|e| [e,e.to_s]}.to_h
      end
    end
  end
end

Array.include CoreExtensions::Array::ActiveSupportHelper